************************************************************************
* Welcome and thank you for downloading the Drupal LiveEnsure module! *
************************************************************************

----------------------------
        Installation
----------------------------
Extract the downlaoded zip, you will find one folder called "exp" and one zip called "liveensure.zip".

Upload the whole "exp" folder to the root directory of your Drupal installation, for example, if your main install resides at  "/var/www/html/drupal" then the exp location would be "/var/www/html/drupal/exp"

Log into the Drupal administrator console with your username and password.

The user you log into the admin console with must have a valid email address. LiveEnsure authentication will use this address.
    - If you're not sure of this address, while logged into the administrator console,  click the People link in the top navigation links, then click the edit button for your user to verify the email address that is configured. 
    - This email ID would be taken by the module to send PIN notifications

Go to Modules in the top navigation and then click on "Install new module", upload the "liveensure.zip" . After installing, activate the module.

Once you activate the module you will find a configuration link for LiveEnsure under the module list, at the bottom.

--------------------
   Configuration
--------------------
You will see a new tab in admin section called "LiveEnsure"

Enter the values needed for LiveEnsure (Host URL, Instance ID, API Key, API Password)
Log into the LiveEnsure Consumer portal and go to the Agent tab to reveal these credentials
The Host URL should be the base url, e.g. https://app.liveensure.com/live-identity/idr
 
-----------------  
Checking Process
-----------------

To test the login process, log out

Log back in as the same admin user and after the normal username/password box, you should get a LE QR code

You can either scan the code with a mobile phone (Android,iPhone,Windows) or click the code to get the email PIN agent (light agent)


---------------
TROUBLESHOOTING
---------------

If you accidentally break your LiveEnsure plugin installation for any reason, and you keep getting challenged with a LiveEnsure QR code on every page, you can disable the plugin in the following way:

- Go to drupal admin>modules and disable this module

If above step do not resolve the issue then follow the following steps

- On the web server where Drupal is installed, go to /sites/all/modules/ and open liveensure.module file
- at line number around 105  you will find 
function liveensure_init() {
 global $base_url;
 //echo variable_get('liveensure_api_session').'<hr>';
 if(arg(0)=="admin"){
	$in_session = variable_get('liveensure_api_session');
	if(arg(1)!="" && arg(1)!="liveensure" && $in_session==2){
		drupal_goto('user/logout');
	}
 }
}
Replace it with
/*
function liveensure_init() {
 global $base_url;
 //echo variable_get('liveensure_api_session').'<hr>';
 if(arg(0)=="admin"){
	$in_session = variable_get('liveensure_api_session');
	if(arg(1)!="" && arg(1)!="liveensure" && $in_session==2){
		drupal_goto('user/logout');
	}
 }
}
*/
- Go to drupal admin>modules and disable this module

---------------
NOTE
---------------
Line number mentioned above may differ and is based off of a default Drupal installation. If you are using a custom Drupal page then please contact your developer to make this modification.

